console.log("Hello world");

// [SECTION] Functions
// Functions in Javascript are lines/block of coded that tell our device/application to perform a certain task when called/invoked.
// They are also used to prevent repeating lines/block of codes that perform the same task/ function

// Function Declaration
	// function statement defines a function with parameters

// function keyword - use to define a javascript function
// function name - is used so we are able to call/invoke a declared function
// function code block ({}) - the statement which compromise the body of the function. this is where the code to be executed.

// Function declaration  // function name
// function name requires open and close parenthesis beside it 


function printname() { //code block
	console.log("My Name is Rom")
};

printname()

// [HOISTING]
// Hoisting is a JavaScript's behavior for certain variables 
// we can call before and after our function declaration


declaredFunction();
// make sure the function is existing whenever we call/invoke a function

function declaredFunction(){
	console.log("Hello World")
};

declaredFunction();

// [Function Expession]
// A function can be also stored in a variable. That is call as function expression
// we cant call before our function declaration
// Hoisting is allowed in function declaration, while we could not hoist through function expression.
let variableFunction = function (){
	console.log("Hello again")
};
variableFunction();

let funcExpression = function funcName(){
	console.log("Hello from the other side")
};
funcExpression();
// funcName();
// Whenever we are calling a named function stored in variable, we just call the variable name, not the function name 

console.log("======================");
console.log("[Reassigning Function]");

declaredFunction();

declaredFunction = function(){
	console.log("updated declaredFunction.")
};
declaredFunction();

funcExpression = function(){
	console.log("updated funcExpression")
};
funcExpression();

// Constant function

const constantFunction = function(){
	console.log("Initialized with const")
};
constantFunction();

// constantFunction = function(){
// 	console.log("New Value")
// };
// constantFunction();
// Function with const keyword cannot be reassigned.
// Scope is the accessibility / visibility of a variables in the code.

/*
	Javascript varibles has 3 types of scope:
	1. Local / block scope
	2. Global scope
	3. Function scope
*/
console.log("========================");
console.log("[Function Scoping]");

// local variable
{
	let localVar = "Armando Perez";
	console.log(localVar);
}
// global variable
// console.log(localVar);

let globalVar = "Mr. Variable";
	console.log(globalVar);
// Global variable can be access by local variable
{
	console.log(globalVar);
}	

// [Function Scoping]
// Variable defined inside a function are not accessible / visible outside the function 
// Variables declared with var,let and const are quite similar when declared inside a function.
function showname(){
	var functioVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";
	console.log(functioVar);
	console.log(functionConst);
	console.log(functionLet);

}
showname();
// Error - This are function scoped variable and cannot be accessed outside the function they were declared in 
	// console.log(functioVar);
	// console.log(functionConst);
	// console.log(functionLet);

// Nested function
	// You can create another function inside a function, called Nested Function 

function myNewFunction(){
	let name = "Jay";

	function nestedFunction(){
		let nestedName = "John"
		console.log(name);
	}

	nestedFunction();//result not defined function
	console.log(nestedFunction); 
};
myNewFunction();


// Global Scoped Variable 
let globalName = "Zuitt";
function myNewFunction2(){
	let nameInside = "Renz";
	console.log(globalName);
};
myNewFunction2();

// alert() allows us to show a small window at the top of our browser page to show information to our users.
// alert("You cannot access");

function showSampleAlert(){
	alert("Hello, user");
};

showSampleAlert();

// Alert messages inside a function will only execute whenever we call/ invoke the function 

console.log("I will only log in the console when the alert is dismissed");
// Notes on the use of alert();
	// Show only an alert for short dialog/messages to the user
	// Do not overuse alert() because the program/js has to wait for it to be dismised before continuing.

// [Prompt]
	// prompt() allow us to show small window at the top of the browser to gather user input.


// let samplePrompt = prompt("Enter your full name");
// // console.log(samplePrompt);

// console.log(typeof samplePrompt);

// console.log(" Hello "+ samplePrompt);

function printWelcomeMessage(){
	let firstName = prompt("Enter you First Name: ");
	let lastName = prompt("Enter you Last Name: ");
	console.log("Hello, " + firstName + " " + lastName );
	console.log("Welcome to my page!");
};
printWelcomeMessage();

// [SECTION] Function Naming Convention

// Function name should be definitive of th task it will perform. It usually contains a verb.


function getCourses(){
	let courses = ["Science 101","Math 101", "English 101" ];
	console.log(courses);
};
getCourses();

// Avoid generic names to avoid confusion within our code.

function get(){
	let name = "Jamie";
	console.log(name);
};
get();

// Avoid pointless and inappropriate function names, example: foo, bar, etc.

function foo(){
	console.log("25%5");
};
foo();

// Name your function in small caps. Follow camelCase when naming variables and functions

function displayCarIfor(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1.5M");
};
displayCarIfor();